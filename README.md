
# braconieR

<!-- badges: start -->
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
<!-- badges: end -->

The goal of braconieR is to find and annotate bracoviral regions in
genome assemblies of braconids wasps.

> :zap: This software is not ready for wider usage. Use at your own
> risk. If you want to see the list of features intended, consult the
> [issues list](https://gitlab.com/hrz-project/braconier/-/issues).
> :zap:

BraconieR = braco in r. (*Un braconier* is a poacher in French.)

## Installation

### from source

You can install the source code of braconieR from [source](https://gitlab.com/hrz-project/braconier) with:

``` r
devtools::install_gitlab("hrz-project/braconier")
```

### prebuilt package (recommended)

``` shell
wget "https://gitlab.com/hrz-project/braconier/uploads/f9fc6f9478fbba3fadb373de358c8f28/braconieR_0.0.1.tar.gz"
R CMD INSTALL braconieR_0.0.1.tar.gz
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(braconieR)
## basic example code
```

## Documentation

See an example usage at https://hrz-project.gitlab.io/braconier/index.html, or use

``` r
browseVignettes(package = "braconieR")
```

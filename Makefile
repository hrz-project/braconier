# prepare the package for release
PKGNAME := $(shell sed -n "s/Package: *\([^ ]*\)/\1/p" DESCRIPTION)
PKGVERS := $(shell sed -n "s/Version: *\([^ ]*\)/\1/p" DESCRIPTION)
PKGSRC  := $(shell basename `pwd`)
PKGTAR  := $(PKGNAME)_$(PKGVERS).tar.gz


all: check clean

.PHONY: site
site: public/index.html

public/index.html: vignettes/demonstration.html
	@mkdir -p public
	cp $< public/index.html

.PHONY: docs
docs:
	R -q -e 'devtools::document()'

build:
	R CMD build --no-manual --no-build-vignettes .

build-full:
	R CMD build .

install: build
	R CMD INSTALL $(PKGTAR)

install-full: build-full
	R CMD INSTALL --build $(PKGTAR)

check: build-full
	R CMD check $(PKGTAR) --as-cran


# examples:
.PHONY: vignettes
vignettes:
	$(MAKE) -C vignettes

vignettes/demonstration.html:
	$(MAKE) -C vignettes demonstration.html

clean:
	$(RM) -r $(PKGNAME).Rcheck/
